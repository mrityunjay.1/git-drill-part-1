Drills Part 1:


1. Create the following directory structure. (Create empty files where necessary)

hello - mkdir hello
├── five - mkdir five
│   └── six - mkdir six
│       ├── c.txt - cat > c.txt
│       └── seven - mkdir seven
│           └── error.log - cat > error.log
└── one - mkdir one
    ├── a.txt - cat > a.txt
    ├── b.txt - cat > b.txt
    └── two - mkdir two
        ├── d.txt - cat > d.txt
        └── three - mkdir three
            ├── e.txt - cat > e.txt
            └── four - mkdir four
                └── access.log - cat > access.log



2. Delete all the files with the .log extension

Find . -name "*.log" -delete

3. Add the following content to a.txt

cat > a.txt

Unix is a family of multitasking, multiuser computer operating systems that derive from the original AT&T Unix, development starting in the 1970s at the Bell Labs research center by Ken Thompson, Dennis Ritchie, and others.

4. Delete the directory named five.

rm -rf five
